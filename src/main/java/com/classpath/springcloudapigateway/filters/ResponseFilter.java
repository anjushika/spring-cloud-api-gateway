package com.classpath.springcloudapigateway.filters;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.List;

@Component
public class ResponseFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        HttpHeaders httpHeaders = exchange.getRequest().getHeaders();
        List<String> values = httpHeaders.get("request_id");

        if (values != null && !values.isEmpty()) {
            System.out.println("Does the request contains the request_id "+ values.isEmpty());
            exchange.getResponse().getHeaders().add("request_id", values.get(0));
        }
        return chain.filter(exchange);
    }
}